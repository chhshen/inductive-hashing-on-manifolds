Code for the paper `Inductive Hashing on Manifolds`
------------------------------------------------

..by Fumin Shen, Chunhua Shen, Qinfeng Shi, Anton van den Hengel, Zhenmin Tang.

..In: IEEE Conf. Computer Vision and Pattern Recognition (CVPR2013)


..The paper can be downloaded at [http://arxiv.org/abs/1303.7043](http://arxiv.org/abs/1303.7043)

Run 'demo_IMH.m' for a demo.
--------------------------

Any questions/comments are welcome (fumin.shen[at]gmail.com, chhshen[at]gmail.com).


If you use this code in your research paper, please cite our CVPR2013 paper: 

```
  @inproceedings{CVPR13FShen,
     author      = {Fumin Shen and Chunhua Shen and Qinfeng Shi and Anton {van den Hengel} and Zhenmin Tang},
     title       = {Inductive Hashing on Manifolds},
     year        = {2013},
     month       = {},
     booktitle   = {IEEE Conference on Computer Vision and Pattern Recognition (CVPR'13)},
     address     = {Oregon, USA},
     pages       = {},
     volume      = {},
     publisher   = {IEEE},
     url         = {http://arxiv.org/abs/1303.7043},
  } 
```

